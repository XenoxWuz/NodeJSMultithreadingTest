const assert = require('assert')
const { Worker, MessageChannel } = require('worker_threads')

var arraySize = 1000000

var array = [...Array(arraySize).keys()]
threadCount = 8
threadArraySize = array.length / threadCount
var start = 0
var ende = threadArraySize

workerArray = []

var resultArray = []
var finished = 0

var startTime = Date.now()

for (let i = 0; i < array.length; i++) {
    array[i] = ((array[i] + 1) * 951654651 - 21345) / 951654
}

var finishTime = Date.now()
var timeUsed = finishTime - startTime
console.log('Normal: ' + timeUsed)

array = [...Array(arraySize).keys()]
resultArray = []

startTime = Date.now()

for (let i = 0; i < threadCount; i++) {
    const worker = new Worker('./worker.js', { workerData: { array: array, start: start, ende: ende } })
    const { port1, port2 } = new MessageChannel();

    port1.on('message', (message) => {
        console.log(message)
    })

    worker.postMessage({ port1: port2 }, [port2])
    /*
        worker.once('message', (arraySlice) => {
            resultArray = resultArray.concat(arraySlice)
        })
    
        worker.once('exit', () => {
            finished = finished + 1
            if (finished === threadCount) {
                weiter()
            }
        })
    */

    workerArray.push(worker)

    start = ende
    ende = ende + threadArraySize
}

setInterval(test, 1000)

function test() {
    workerArray.forEach(element => {
        element.postMessage('neu')
    });
}

function weiter() {
    finishTime = Date.now()
    timeUsed = finishTime - startTime
    console.log('Multithreaded: ' + timeUsed)
}